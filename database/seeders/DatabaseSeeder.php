<?php

namespace Database\Seeders;

use App\Models\Customer;
use App\Models\Session;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Customer::factory()
            ->count(3)
            ->has(User::factory()
                    ->count(100)
                    ->has(
                        Session::factory()->count(5)
                    ))
            ->create();
    }
}

<?php

namespace Database\Factories;

use App\Models\Customer;
use App\Models\Invoice;
use Illuminate\Database\Eloquent\Factories\Factory;

class InvoiceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Invoice::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'start_date' => $this->faker->date(),
            'end_date' => $this->faker->date(),
            'customer_id' => Customer::factory(),
            'price' => $this->faker->randomNumber(),
            'total_registrations' => $this->faker->randomNumber(),
            'total_activations' => $this->faker->randomNumber(),
            'total_appointments' => $this->faker->randomNumber(),
        ];
    }
}

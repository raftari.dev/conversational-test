<?php

namespace Database\Factories;

use App\Models\Customer;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'email' => $this->faker->safeEmail,
            'customer_id' => Customer::factory(),
            'invoiced_for_registration' => $this->faker->boolean,
            'invoiced_for_activation' => $this->faker->boolean,
            'invoiced_for_appointment' => $this->faker->boolean,
            'created_at' => $this->faker->dateTimeBetween('-3 month', 'now'),
        ];
    }
}

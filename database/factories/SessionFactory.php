<?php

namespace Database\Factories;

use App\Models\Session;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class SessionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Session::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'user_id' => User::factory(),
            'activated' => rand(0, 1) == 1 ? $this->faker->dateTimeBetween('-90 days', 'now') : null,
            'appointment' => rand(0, 1) == 1 ? $this->faker->dateTimeBetween('-90 days', 'now') : null,
        ];
    }
}

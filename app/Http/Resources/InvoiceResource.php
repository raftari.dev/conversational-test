<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'start_date' => $this->start_date->format('Y-m-d'),
            'end_date' => $this->end_date->format('Y-m-d'),
            'customer' => CustomerResource::make($this->customer),
            'total_registrations' => $this->total_registrations,
            'total_activations' => $this->total_activations,
            'total_appointments' => $this->total_appointments,
            'price' => round($this->price / 100, 2),
            'events' => EventResource::collection($this->events),
            'users' => UserInvoiceResource::collection($this->users),
        ];
    }
}

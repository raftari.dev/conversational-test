<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'event_type' => $this->event_type,
            'happened_at' => $this->happened_at,
            'user' => UserResource::make($this->user),
            'price' => round($this->price / 100, 2),
        ];
    }
}

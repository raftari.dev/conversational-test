<?php

namespace App\Http\Controllers;

use App\Http\Requests\InvoiceStoreRequest;
use App\Http\Resources\InvoiceResource;
use App\Models\Customer;
use App\Models\Invoice;
use App\Services\InvoiceGeneratorService;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    public function store(InvoiceStoreRequest $request)
    {
        $customer = Customer::find($request->customer_id);
        $invoice = (new InvoiceGeneratorService($customer, $request->start_date, $request->end_date))->generate();
        $invoice->load(['events', 'events.user', 'users']);

        return new InvoiceResource($invoice);
    }

    public function show(Request $request, Invoice $invoice): \App\Http\Resources\InvoiceResource
    {
        $invoice->load(['events', 'events.user', 'users']);

        return new InvoiceResource($invoice);
    }
}

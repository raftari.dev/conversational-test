<?php

namespace App\Services;

use App\Models\Event;
use App\Models\User;

class InvoiceEventGeneratorService
{
    private $users;

    public function generate(array $data): array
    {
        $generatedData = [];
        $this->users = User::find(collect($data)->pluck('user_id')->unique()->toArray());

        foreach ($data as $eventData) {
            $generatedData[] = Event::make(array_merge(['price' => $this->calculatePrice($eventData['event_type'], $eventData['user_id'])], $eventData));
        }

        return $generatedData;
    }

    private function calculatePrice(string $eventType, int $userId): int
    {
        switch ($eventType) {
            case 'appointment':
                return $this->calculateAppointmentPrice($userId);
            case 'activation':
                return $this->calculateActivationPrice($userId);
            case 'registration':
                return $this->calculateRegistrationPrice($userId);
            default:
                return 0;
        }
    }

    private function calculateAppointmentPrice(int $userId)
    {
        $user = $this->users->find($userId);
        if ($user->invoiced_for_appointment) {
            return 0;
        }
        $this->users->find($userId)->update(['invoiced_for_appointment' => true]);
        $basePrice = config('invoice.price.appointment');
        if ($user->invoiced_for_activation) {
            return $basePrice - config('invoice.price.activation');
        }
        if ($user->invoiced_for_registration) {
            return $basePrice - config('invoice.price.registration');
        }

        return $basePrice;
    }

    private function calculateActivationPrice(int $userId)
    {
        $user = $this->users->find($userId);
        if ($user->invoiced_for_activation || $user->invoiced_for_appointment) {
            return 0;
        }
        $this->users->find($userId)->update(['invoiced_for_activation' => true]);
        $basePrice = config('invoice.price.activation');
        if ($user->invoiced_for_registration) {
            return $basePrice - config('invoice.price.registration');
        }

        return $basePrice;
    }

    private function calculateRegistrationPrice(int $userId)
    {
        $user = $this->users->find($userId);

        if ($user->invoiced_for_registration || $user->invoiced_for_activation || $user->invoiced_for_appointment) {
            return 0;
        }
        $this->users->find($userId)->update(['invoiced_for_registration' => true]);

        return config('invoice.price.registration');
    }
}

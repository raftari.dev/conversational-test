<?php

namespace App\Services;

use App\Models\Invoice;
use App\Support\InvoiceGatheredData;
use Illuminate\Support\Collection;

class InvoiceDataGathererService
{
    public function __construct(private Invoice $invoice)
    {
    }

    public function gather(): InvoiceGatheredData
    {
        $appointmentEvents = $this->getAppointmentEvents();
        $activationEvents = $this->getActivationEvents();
        $usersWithRegistration = $this->getUsersWithRegistration();
        $mergedUsersData = $this->getUsersInvoiceData($usersWithRegistration, $activationEvents, $appointmentEvents);
        $mergedEventsData = $this->getEventsData($usersWithRegistration, $activationEvents, $appointmentEvents);

        return new InvoiceGatheredData(
            $usersWithRegistration->count(),
            $activationEvents->count(),
            $appointmentEvents->count(),
            $mergedUsersData,
            $mergedEventsData
        );
    }

    private function getUsersWithRegistration(): Collection
    {
        return $this->invoice->customer
            ->users()
            ->whereBetween('created_at', [$this->invoice->start_date, $this->invoice->end_date])
            ->orderBy('created_at')
            ->pluck('created_at', 'id');
    }

    private function getActivationEvents(): Collection
    {
        return $this->invoice->customer
            ->sessions()
            ->whereBetween('activated', [$this->invoice->start_date, $this->invoice->end_date])
            ->orderBy('activated')
            ->get(['user_id', 'activated']);
    }

    private function getAppointmentEvents(): Collection
    {
        return $this->invoice->customer
            ->sessions()
            ->whereBetween('appointment', [$this->invoice->start_date, $this->invoice->end_date])
            ->orderBy('appointment')
            ->get(['user_id', 'appointment']);
    }

    private function getEventsData(Collection $registeredUsers, Collection $activationEvents, Collection $appointmentEvents): array
    {
        $mergedEvents = [];
        foreach ($appointmentEvents as $event) {
            $mergedEvents[] = [
                'user_id' => $event->user_id,
                'event_type' => 'appointment',
                'happened_at' => $event->appointment,
            ];
        }
        foreach ($activationEvents as $event) {
            $mergedEvents[] = [
                'user_id' => $event->user_id,
                'event_type' => 'activation',
                'happened_at' => $event->activated,
            ];
        }
        foreach ($registeredUsers as $userId => $registeredAt) {
            $mergedEvents[] = [
                'user_id' => $userId,
                'event_type' => 'registration',
                'happened_at' => $registeredAt->format('Y-m-d'),
            ];
        }

        return $mergedEvents;
    }

    private function getUsersInvoiceData(Collection $registeredUsers, Collection $activationEvents, Collection $appointmentEvents): array
    {
        $activatedUsers = $activationEvents->groupBy('user_id')
            ->map(fn ($item) => collect($item)->map(function ($item) {
                return $item->activated;
            }));

        $usersWithAppointment = $appointmentEvents->groupBy('user_id')
            ->map(fn ($item) => collect($item)->map(function ($item) {
                return $item->appointment;
            }));


        $mergedUsersData = [];
        foreach ($registeredUsers as $userId => $createdAt) {
            $mergedUsersData[$userId] = [
                'registration' => $createdAt->format('Y-m-d'),
                'activated' => $activatedUsers->get($userId) ?? [],
                'appointment' => $usersWithAppointment->get($userId) ?? [],
            ];
        }
        foreach ($activatedUsers as $userId => $activated) {
            if (!isset($mergedUsersData[$userId])) {
                $mergedUsersData[$userId] = [
                    'registration' => null,
                    'activated' => $activated,
                    'appointment' => $usersWithAppointment->get($userId) ?? [],
                ];
            }
        }
        foreach ($usersWithAppointment as $userId => $appointment) {
            if (!isset($mergedUsersData[$userId])) {
                $mergedUsersData[$userId] = [
                    'registration' => null,
                    'activated' => [],
                    'appointment' => $appointment,
                ];
            }
        }

        return $mergedUsersData;
    }
}

<?php

namespace App\Services;

use App\Models\User;

class UserInvoiceGeneratorService
{
    public function generate(array $data, array $invoiceEvents): array
    {
        $generatedData = [];
        $users = User::find(array_keys($data));
        foreach ($data as $id => $userInvoiceData) {
            $generatedData[$id] = [
                'price' => collect($invoiceEvents)->where('user_id', $id)->sum('price'),
                'data' => json_encode($userInvoiceData),
            ];
        }

        return $generatedData;
    }

    private function calculatePrice(User $user, bool $hasRegistration, bool $hasActivation, bool $hasAppointment): int
    {
        if ($hasAppointment) {
            return $this->calculateAppointmentPrice($user);
        }
        if ($hasActivation) {
            return $this->calculateActivationPrice($user);
        }
        if ($hasRegistration) {
            return $this->calculateRegistrationPrice($user);
        }

        return 0;
    }

    private function calculateAppointmentPrice(User $user)
    {
        if ($user->invoiced_for_appointment) {
            return 0;
        }
        // $user->update(['invoiced_for_appointment' => true]);
        $basePrice = config('invoice.price.appointment');
        if ($user->invoiced_for_activation) {
            return $basePrice - config('invoice.price.activation');
        }
        if ($user->invoiced_for_registration) {
            return $basePrice - config('invoice.price.registration');
        }

        return $basePrice;
    }

    private function calculateActivationPrice(User $user)
    {
        if ($user->invoiced_for_activation) {
            return 0;
        }
        // $user->update(['invoiced_for_activation' => true]);
        $basePrice = config('invoice.price.activation');
        if ($user->invoiced_for_registration) {
            return $basePrice - config('invoice.price.registration');
        }

        return $basePrice;
    }

    private function calculateRegistrationPrice(User $user)
    {
        if ($user->invoiced_for_registration) {
            return 0;
        }
        // $user->update(['invoiced_for_registration' => true]);

        return config('invoice.price.registration');
    }
}

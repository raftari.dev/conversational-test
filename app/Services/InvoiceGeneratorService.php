<?php

namespace App\Services;

use App\Models\Customer;
use App\Models\Invoice;

class InvoiceGeneratorService
{
    private Invoice $invoice;

    public function __construct(Customer $customer, string $start_date, string $end_date)
    {
        $this->invoice = Invoice::make([
            'customer_id' => $customer->id,
            'start_date' => $start_date,
            'end_date' => $end_date,
        ]);
    }

    public function generate(): Invoice|array
    {
        $invoiceGatheredData = (new InvoiceDataGathererService($this->invoice))->gather();
        $invoiceEvents = (new InvoiceEventGeneratorService())->generate($invoiceGatheredData->eventsData);
        $userInvoices = (new UserInvoiceGeneratorService())->generate($invoiceGatheredData->userInvoiceData, $invoiceEvents);

        $this->invoice->fill([
            'total_registrations' => $invoiceGatheredData->totalRegistrations,
            'total_activations' => $invoiceGatheredData->totalActivations,
            'total_appointments' => $invoiceGatheredData->totalAppointments,
            'price' => collect($userInvoices)->sum('price')
        ])->save();

        $this->invoice->users()->sync($userInvoices);
        $this->invoice->events()->saveMany($invoiceEvents);

        return $this->invoice;
    }
}

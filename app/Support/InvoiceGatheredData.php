<?php

namespace App\Support;

class InvoiceGatheredData
{
    public function __construct(
        public int $totalRegistrations,
        public int $totalActivations,
        public int $totalAppointments,
        public array $userInvoiceData,
        public array $eventsData
    ) {
    }
}

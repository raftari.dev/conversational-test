<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\Customer;
use App\Models\Invoice;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use JMac\Testing\Traits\AdditionalAssertions;
use Tests\TestCase;

/**
 * @see \App\Http\Controllers\InvoiceController
 */
class InvoiceControllerTest extends TestCase
{
    use AdditionalAssertions;
    use RefreshDatabase;
    use WithFaker;

    /**
     * @test
     */
    public function store_uses_form_request_validation(): void
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\InvoiceController::class,
            'store',
            \App\Http\Requests\InvoiceStoreRequest::class
        );
    }

    /**
     * @test
     */
    public function store_saves(): void
    {
        $date1 = $this->faker->date();
        $date2 = $this->faker->date();
        $customer = Customer::factory()->create();

        $response = $this->post(route('invoice.store'), [
            'start_date' => min($date1, $date2),
            'end_date' => max($date1, $date2),
            'customer_id' => $customer->id,
        ]);

        $invoices = Invoice::query()
            ->where('customer_id', $customer->id)
            ->get();
        $this->assertCount(1, $invoices);

        $response->assertCreated();
        $response->assertJsonStructure([]);
    }

    /**
     * @test
     */
    public function show_behaves_as_expected(): void
    {
        $invoice = Invoice::factory()->create();

        $response = $this->get(route('invoice.show', $invoice));

        $response->assertOk();
        $response->assertJsonStructure([]);
    }
}

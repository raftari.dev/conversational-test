<?php

namespace Tests\Unit\Services;

use App\Models\Customer;
use App\Models\Session;
use App\Models\User;
use App\Services\InvoiceGeneratorService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class InvoiceGeneratorServiceTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    /**
     * @test
     * @group invoice-generator-service
     */
    public function correct_price_calculation_validation(): void
    {
        config([
            'invoice.price.appointment' => 399,
            'invoice.price.activation' => 99,
            'invoice.price.registration' => 49,
        ]);

        $customer = Customer::factory()->create();

        $user1 = User::factory()->create([
            'customer_id' => $customer->id,
            'created_at' => '2020-12-01  00:00:00',
            'invoiced_for_registration' => false,
            'invoiced_for_activation' => false,
            'invoiced_for_appointment' => false,
        ]);
        $user2 = User::factory()->create([
            'customer_id' => $customer->id,
            'created_at' => '2020-12-05  00:00:00',
            'invoiced_for_registration' => false,
            'invoiced_for_activation' => false,
            'invoiced_for_appointment' => false,
        ]);
        $user3 = User::factory()->create([
            'customer_id' => $customer->id,
            'created_at' => '2021-01-01  00:00:00',
            'invoiced_for_registration' => false,
            'invoiced_for_activation' => false,
            'invoiced_for_appointment' => false,
        ]);
        $user4 = User::factory()->create([
            'customer_id' => $customer->id,
            'created_at' => '2020-09-01  00:00:00',
            'invoiced_for_registration' => false,
            'invoiced_for_activation' => false,
            'invoiced_for_appointment' => false,
        ]);


        $user1->sessions()->saveMany([
            Session::make([
                'activated' => '2021-01-15',
                'appointment' => null,
            ]),
            Session::make([
                'activated' => '2021-01-18',
                'appointment' => null,
            ]),
        ]);

        $user2->sessions()->saveMany([
            Session::make([
                'activated' => null,
                'appointment' => '2021-01-15',
            ]),
        ]);

        $user3->sessions()->saveMany([
            Session::make([
                'activated' => '2021-01-10',
                'appointment' => null,
            ]),
        ]);

        $user4->sessions()->saveMany([
            Session::make([
                'activated' => '2020-10-11',
                'appointment' => '2020-12-27',
            ]),
            Session::make([
                'activated' => '2021-01-12',
                'appointment' => '2021-01-22',
            ]),
        ]);

        $oldInvoice = (new InvoiceGeneratorService($customer, '2020-01-01', '2020-12-31'))->generate();
        $this->assertTrue($user1->refresh()->invoiced_for_registration);
        $this->assertTrue($user2->refresh()->invoiced_for_registration);
        $this->assertFalse($user3->refresh()->invoiced_for_registration || $user3->invoiced_for_activation || $user3->invoiced_for_appointment);
        $this->assertTrue($user4->refresh()->invoiced_for_appointment);

        $newInvoice = (new InvoiceGeneratorService($customer, '2021-01-01', '2021-02-01'))->generate();

        $user1_invoice = $newInvoice->users()->where('user_id', $user1->id)->first();
        $user2_invoice = $newInvoice->users()->where('user_id', $user2->id)->first();
        $user3_invoice = $newInvoice->users()->where('user_id', $user3->id)->first();
        $user4_invoice = $newInvoice->users()->where('user_id', $user4->id)->first();

        $this->assertEquals($user1_invoice->pivot->price, 50);
        $this->assertEquals($user2_invoice->pivot->price, 350);
        $this->assertEquals($user3_invoice->pivot->price, 99);
        $this->assertEquals($user4_invoice->pivot->price, 0);
        $this->assertEquals($newInvoice->price, 499);
    }
}

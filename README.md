# Senior Backend Developer Project For Conversational
This projects is written in PHP 8 and Laravel.
Preferred Database for running teh projects is MySql.
Clone project on your desired location:
```
git clone https://gitlab.com/raftari.dev/conversational-test.git
```

## Run Project with Docker

First remember to install docker on your machine.
Secondly you need to install docker-compose.
Third you need to install composer.

The project uses Laravel Sail, which is based on Docker.
You can run the project by using the following command in base directory.

```
composer install
cp .env.example .env
php artisan key:generate
./vendor/bin/sail up
```

If everything is ok, you can see Laravel in your browser at <http://localhost>.

It is recommended to add following alias to your command line:

```
alias sail='./vendor/bin/sail'
```

Then you can interact with the project's container using artisan commands like this:

```
sail artisan migrate:refresh --seed
```

using above command, tables will be created and seeded.

## Test project

There is a `.env.testing` file for testing the project. If you wanted to test the project with PHPUnit, you can run the following command:

```
touch database/database.sqlite
php artisan test
```

## Apis

For Apis, there's a postman collection to use:

```
https://go.postman.co/workspace/Conversational~b471f621-de5f-4f61-9e32-5019f87380e8/collection/2737985-6eca738c-7975-4029-89b8-e60f1d58f6a3
```

and following online document:

```
https://documenter.getpostman.com/view/2737985/UVC2F8jJ
```


<?php

return [
    'price' => [
        'appointment' => env('INVOICE_APPOINTMENT_PRICE', 399),
        'registration' => env('INVOICE_REGISTRATION_PRICE', 49),
        'activation' => env('INVOICE_ACTIVATION_PRICE', 99),
    ]

];
